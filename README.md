# txt-wiz-cli

cli tool for basic text manipulation in C++

## How to run the program:
1. Open your terminal where the README.md file is
1. Run the following command: **cd .build**
1. Run the following command and wait for the program to compile: **cmake ..**
1. Run the following command and wait for the program to compile: **make**
1. Now you can run the program with the following command: **./txt-wiz-cli**
