#include "pandasays.hpp"
#include <string>
#include <iostream> // For std::cout
#include "CustomExceptions.hpp"
#include <iomanip> // For std::setw and std::left

///////////////////////////////////////////////////////////

using std::cout;

///////////////////////////////////////////////////////////

void pandasays::execute() const
{
    // Prints border
    cout << "----------------------------------------------------\n";
    // Each line is 50 characters wide (+2 for side borders)
    for (size_t i = 0; i < sayThis.length(); i += 50)
    {
        // Prints line and side borders
        cout << '|' << std::left << std::setw(50) << sayThis.substr(i, 50) << "|\n";
    }
    // Prints border
    cout << "----------------------------------------------------\n";
    // Prints panda wannabe
    cout << "\\ |\n";
    cout << " \\|\n";
    cout << " __         __\n";
    cout << "/●●\\.-\"\"\"-./●●\\\n";
    cout << "\\●●● -   - ●●●/\n";
    cout << " |   ━   ━   |   \n";
    cout << " \\  .-'''-.  /\n";
    cout << "  '-\\__◍__/-'\n";
    cout << "     `---`   I'm a panda, believe me :p\n";
}

///////////////////////////////////////////////////////////

void pandasays::setSayThis(const std::string& sayThis)
{
    // making sure none of the strings are empty
    if (sayThis.empty())
    {
        throw emptyStringException();
    }
    
    this->sayThis = sayThis;
}