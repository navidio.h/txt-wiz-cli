#include "split.hpp"
#include <string>
#include "CustomExceptions.hpp"
#include <iostream> // For std::cout

///////////////////////////////////////////////////////////

void split::setInputAndDelimiter(const std::string& input, const std::string& delimiter)
{
    // making sure none of the strings are empty
    if (input.empty() || delimiter.empty())
    {
        throw emptyStringException();
    }
    
    this->input = input;
    this->delimiter = delimiter;
}

///////////////////////////////////////////////////////////

void split::execute() const
{
    std::cout << "{";
    for (size_t i = 0; i < input.length(); i++)
    {
        // First similarity found
        if (input[i] == delimiter[0])
        {
            // Checks if the input string is long enough to fit the delimiter
            if (i + delimiter.length() <= input.length())
            {
                // Checks if the delimiter exists in input string
                if (input.substr(i, delimiter.length()) == delimiter)
                {
                    std::cout << ", ";
                    // Adjusting position
                    i += delimiter.length() - 1;
                    continue;
                }
                
            }
            
            // Checking if delimiter is at the end of input string
            else if (i + delimiter.length() == input.length())
            {
                // no ', ' needed
                break;
            }
            
        }
        // Printing a char from the input string
        std::cout << input[i];

    }
    
    std::cout << "}\n";
}
