#include "find.hpp"
#include <string>
#include "CustomExceptions.hpp"
#include <iostream> // for std::cout

///////////////////////////////////////////////////////////

void find::execute() const
{
    /* True if there is enough room for the key string
    in the main string (before the first similarity) */
    bool before;
    /* True if there are no differences between the main string
    and the key string after the first similarity */
    bool after;
    for (int i = 0; i < mainString.length(); i++)
    {
        for (int j = 0; j < keyString.length(); j++)
        {
            // First similarity found
            if (mainString[i] == keyString[j])
            {
                before = true;
                after = true;
                // Not enough room for the key string in the main string (before the first similar char)
                if (i - j < 0)
                {
                    before = false;
                }
                // Not Not enough room for the key string in the main string (after the first similar char)
                if (i + keyString.length() - j > mainString.length())
                {
                    after = false;
                }
                // Checks for differences between the key string and the main string (after the first similar char)
                for (size_t k = j + 1, p = i + 1; k < keyString.length() && after && before; k++, p++)
                {
                    // '-' can be anything but space
                    if (keyString[k] == '-' && mainString[p] == ' ')
                    {
                        after = false;
                    }
                    /* if keyString[k] in not '-',
                    it has to be the same character as mainString[p] */
                    else if (keyString[k] != '-' && keyString[k] != mainString[p])
                    {
                        after = false;
                    }

                }
                // Checks for differences between the key string and the main string (before the first similar char)
                for (int k = j - 1, p = i - 1; k >= 0 && after && before; k--, p--)
                {
                    // '-' can be anything but space
                    if (keyString[k] == '-' && mainString[p] == ' ')
                    {
                        before = false;
                    }
                    /* if keyString[k] in not '-',
                    it has to be the same character as mainString[p] */
                    else if (keyString[k] != '-' && keyString[k] != mainString[p])
                    {
                        before = false;
                    }
                }
                
                // the key string was found in the main string
                if ((after == true) && (before == true))
                {
                    // i - j is where the key string begins in the main string
                    std::cout << mainString.substr(i - j, keyString.length()) << '\n';
                    return;
                }
                
            }
            
        }
        
    }
    // Either before or after were false
    std::cout << "Key string was not found.\n";
}

///////////////////////////////////////////////////////////

void find::setKeyAndMainStrings(const std::string& keyString, const std::string& mainString)
{
    // making sure none of the strings are empty
    if (keyString.empty() || mainString.empty())
    {
        throw emptyStringException();
    }
    
    this->keyString = keyString;
    this->mainString = mainString;
}
