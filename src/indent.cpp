#include "indent.hpp"
#include "iostream" // For std::cout
#include "CustomExceptions.hpp"
#include <string>
#include <fstream> // To read from file

///////////////////////////////////////////////////////////

void indent::execute() const
{
    // Stores lines from user's txt file of choice
    std::string readFromFile;
    // Opens user's txt file of choice
    std::ifstream input(fileName, std::ios::in);
    // -n
    if (flag)
    {
        for (int i = lineNumber; ; i++)
        {
            std::getline(input, readFromFile);
            // Checks if getline was successful
            if (input.fail())
            {
                // EOF reached
                break;
            }
            // prints line
            std::cout << i << " " << indentation << ' ' << readFromFile << '\n';
        }
        
    }
    // no -n
    else
    {
        while (true)
        {
            std::getline(input, readFromFile, '\n');
            // Checks if getline was successful
            if (input.fail())
            {
                // EOF reached
                break;
            }
            // prints line
            std::cout << indentation << ' ' << readFromFile << '\n';
        }
        
    }
    
    input.close();
}

///////////////////////////////////////////////////////////

void indent::setFileNameAndIndentation(const std::string& fileName, const std::string& indentation)
{
    // making sure none of the strings are empty
    if (fileName.empty() || indentation.empty())
    {
        throw emptyStringException();
    }
    // Checks if user's txt file of choice exists and can be opened
    std::ifstream test(fileName, std::ios::in);
    if (test.fail())
    {
        throw nonExistentFileException();
    }
    test.close();

    this->indentation = indentation;
    this->fileName = fileName;
}

///////////////////////////////////////////////////////////

void indent::setFlagAndNumber(const std::string& number)
{
    // making sure none of the strings are empty
    if (number.empty())
    {
        throw emptyStringException();
    }

    for (size_t i = 0; i < number.length(); i++)
    {
        // line numbers can be negative
        if (i == 0 && number[i] == '-')
        {
            continue;
        }
        
        // Checks if the first line number is an integer
        if (!isdigit(number[i]))
        {
            throw nonExistentCommandException();
        }
        
    }
    // -n input by user
    flag = true;
    lineNumber = stoi(number);
}