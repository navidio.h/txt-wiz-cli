#include "capitalize.hpp"
#include <iostream> // For std::cout
#include "CustomExceptions.hpp"
#include <string>

///////////////////////////////////////////////////////////

void capitalize::execute() const
{
    // True if a letter should be capitalized
    bool upperCase = true;
    for (size_t i = 0; i < inputString.length(); i++)
    {
        // If char is {A-z}
        if ((inputString[i] >= 'a' && inputString[i] <= 'z') || (inputString[i] >= 'A' && inputString[i] <= 'Z'))
        {
            // Current char should be capitalized
            if (upperCase)
            {
                // function toupper returns an integer
                std::cout << static_cast<char>(toupper(inputString[i]));
                // Next letter (A-z) should not be capitalized
                upperCase = false;
            }
            // Current char should not be capitalized
            else
            {
                std::cout << static_cast<char>(tolower(inputString[i]));
            }
            
        }

        else // If char is anything but A-z
        {
            std::cout << static_cast<char>(tolower(inputString[i]));
            // If current char is '-' or '_', next char is from the same word and should not be capitalized
            if (inputString[i] != '-' && inputString[i] != '_')
            {
                upperCase = true;
            }

        }

    }

    std::cout << '\n';
}

///////////////////////////////////////////////////////////////////

void capitalize::setInputString(const std::string& inputString)
{
    // making sure input string is not empty
    if (inputString.empty())
    {
        throw emptyStringException();
    }
    
    this->inputString = inputString;
}
