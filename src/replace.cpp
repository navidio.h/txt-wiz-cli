#include "replace.hpp"
#include "CustomExceptions.hpp"
#include <iostream> // For std::cout
#include <string>

///////////////////////////////////////////////////////////

void replace::setAllStrings(const std::string& inputString, const std::string& oldString, const std::string& newString)
{
    // Making sure no string is empty
    if (inputString.empty() || oldString.empty() || newString.empty())
    {
        throw emptyStringException();
    }
    
    this->inputString = inputString;
    this->oldString = oldString;
    this->newString = newString;
}

///////////////////////////////////////////////////////////

void replace::execute() const
{
    for (size_t i = 0; i < inputString.length(); i++)
    {
        // True if a substr from the input string is the same as the old string
        bool same = false;
        if (inputString[i] == oldString[0] && i + oldString.length() <= inputString.length())
        {
            // First similarity found 
            same = true;
            for (size_t j = i + 1; j < inputString.length() && j < oldString.length() && same; j++)
            {
                // Checking chars one by one to find possible differences
                if (inputString[j] != oldString[j])
                {
                    same = false;
                }
                
            }
            
        }
        // New string should be replaced with old string
        if (same)
        {
            // Printing new string
            std::cout << newString;
            // Adjusting position
            i += oldString.length() - 1;
        }
        // No replacement needed
        else
        {
            // Printing a char from the input string
            std::cout << inputString[i];
        }

    }

    std::cout << '\n';
}
