#include <iostream>
#include "command.hpp"
#include "capitalize.hpp"
#include "reverse.hpp"
#include "replace.hpp"
#include "CustomExceptions.hpp"
#include "split.hpp"
#include "evaluate.hpp"
#include "find.hpp"
#include "pandasays.hpp"
#include "indent.hpp"
#include "txtwrap.hpp"
#include "CustomHandlers.hpp"
#include <new> // For std::set_new_handler
#include "diff.hpp"
#include "decrypt.hpp"
#include <string>

///////////////////////////////////////////////////////////

using std::cerr;
using std::cout;

///////////////////////////////////////////////////////////

int main(int argc, char const *argv[])
{
    // Sets custom new handler
    std::set_new_handler(customNewHandler);
    // Used for conversion from const char* (argv[]) to std::string
    std::string temp1;
    std::string temp2;
    // Base class pointer
    command* commandPtr = nullptr;
    try
    {
        // Checks if user's input has the correct number of arguments
        if (argc >= 4 && argc <= 7)
        {
            // string manipulation
            if ((temp1 = argv[1]) == "str")
            {
                if ((temp1 = argv[2]) == "capitalize" && argc == 4)
                {
                    // Allocates memory space for an object of type capitalize
                    commandPtr = new capitalize;
                    dynamic_cast<capitalize*>(commandPtr)->setInputString(argv[3]);
                    commandPtr->execute();
                }

                else if ((temp1 = argv[2]) == "reverse" && argc == 4)
                {
                    // Allocates memory space for an object of type reverse
                    commandPtr = new reverse;
                    dynamic_cast<reverse*>(commandPtr)->setInputString(argv[3]);
                    commandPtr->execute();
                }
                
                else if ((temp1 = argv[2]) == "replace" && argc == 6)
                {
                    // Allocates memory space for an object of type replace
                    commandPtr = new replace;
                    dynamic_cast<replace*>(commandPtr)->setAllStrings(argv[5], argv[3], argv[4]);
                    commandPtr->execute();
                }
                
                else if ((temp1 = argv[2]) == "split" && argc == 5)
                {
                    // Allocates memory space for an object of type split
                    commandPtr = new split;
                    dynamic_cast<split*>(commandPtr)->setInputAndDelimiter(argv[3], argv[4]);
                    commandPtr->execute();
                }

                else if ((temp1 = argv[2]) == "evaluate" && argc == 5)
                {
                    // Allocates memory space for an object of type evaluate
                    commandPtr = new evaluate;
                    dynamic_cast<evaluate*>(commandPtr)->setNumberAndBase(argv[3], argv[4]);
                    commandPtr->execute();
                }
                
                else if ((temp1 = argv[2]) == "find" && argc == 5)
                {
                    // Allocates memory space for an object of type find
                    commandPtr = new find;
                    dynamic_cast<find*>(commandPtr)->setKeyAndMainStrings(argv[3], argv[4]);
                    commandPtr->execute();
                }

                else if ((temp1 = argv[2]) == "pandasays" && argc == 4)
                {
                    // Allocates memory space for an object of type pandasays
                    commandPtr = new pandasays;
                    dynamic_cast<pandasays*>(commandPtr)->setSayThis(argv[3]);
                    commandPtr->execute();
                }
                // Command is invalid
                else
                {
                    throw nonExistentCommandException();
                }
                
            }
            // Text file manipulation
            else if ((temp1 = argv[1]) == "txt")
            {
                if ((temp1 = argv[2]) == "indent" && argc == 5) // No flag
                {
                    // Allocates memory space for an object of type indent
                    commandPtr = new indent;
                    dynamic_cast<indent*>(commandPtr)->setFileNameAndIndentation(argv[3], argv[4]);
                    commandPtr->execute();
                }
                
                else if ((temp1 = argv[2]) == "indent" && (temp2 = argv[5]) == "-n" && argc == 7) // Flag
                {
                    // Allocates memory space for an object of type indent
                    commandPtr = new indent;
                    dynamic_cast<indent*>(commandPtr)->setFileNameAndIndentation(argv[3], argv[4]);
                    dynamic_cast<indent*>(commandPtr)->setFlagAndNumber(argv[6]);
                    commandPtr->execute();
                }
                
                else if ((temp1 = argv[2]) == "txtwrap" && argc == 5) // No flag
                {
                    // Allocates memory space for an object of type txtwrap
                    commandPtr = new txtwrap;
                    dynamic_cast<txtwrap*>(commandPtr)->setFileNameAndWidth(argv[3], argv[4]);
                    commandPtr->execute();
                }
                
                else if ((temp1 = argv[2]) == "txtwrap" && (temp2 = argv[5]) == "-f" && argc == 6) // Flag
                {
                    // Allocates memory space for an object of type txtwrap
                    commandPtr = new txtwrap;
                    dynamic_cast<txtwrap*>(commandPtr)->setFlag();
                    dynamic_cast<txtwrap*>(commandPtr)->setFileNameAndWidth(argv[3], argv[4]);
                    commandPtr->execute();
                }

                else if ((temp1 = argv[2]) == "diff" && argc == 5)
                {
                    // Allocates memory space for an object of type diff
                    commandPtr = new diff;
                    dynamic_cast<diff*>(commandPtr)->setFileNames(argv[3], argv[4]);
                    commandPtr->execute();
                }
                
                else if ((temp1 = argv[2]) == "decrypt" && argc == 6)
                {
                    // Allocates memory space for an object of type decrypt
                    commandPtr = new decrypt;
                    dynamic_cast<decrypt*>(commandPtr)->setAllStrings(argv[3], argv[4], argv[5]);
                    commandPtr->execute();
                }
                // Command is invalid
                else
                {
                    throw nonExistentCommandException();
                }
            }
            // argv[1] != "str" && argv[1] != "txt")
            else
            {
                throw nonExistentCommandException();
            }
            
        }
        // !(argc >= 4 && argc <= 7)
        else
        {
            throw nonExistentCommandException();
        }

    }

    catch(const nonExistentCommandException& NECE)
    {
        cerr << NECE.what() << '\n';
        exit(EXIT_FAILURE);
    }

    catch (const emptyStringException& ESE)
    {
        cerr << ESE.what() << '\n';
        exit(EXIT_FAILURE);
    }

    catch (const nonmatchingBaseAndNumberException& NMBANE)
    {
        cerr << NMBANE.what() << '\n';
        exit(EXIT_FAILURE);
    }

    catch (const nonExistentFileException& NEFE)
    {
        cerr << NEFE.what() << '\n';
        exit(EXIT_FAILURE);
    }

    catch (const terminalSizeException& TSE)
    {
        cerr << TSE.what() << '\n';
        exit(EXIT_FAILURE);
    }

    catch (const outOfWidthException& OOWE)
    {
        cerr << OOWE.what() << '\n';
        exit(EXIT_FAILURE);
    }
    // Frees allocated memory space
    delete commandPtr;
    return 0;
}