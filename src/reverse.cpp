#include "reverse.hpp"
#include <iostream> // For std::cout
#include "CustomExceptions.hpp"

///////////////////////////////////////////////////////////

void reverse::execute() const
{
    // Printing input string in reverse
    for (int i = inputString.length() - 1; i >= 0; i--)
    {
        std::cout << inputString[i];
    }

    std::cout << '\n';
}

///////////////////////////////////////////////////////////

void reverse::setInputString(const std::string& inputString)
{
    // making sure input string is not empty
    if (inputString.empty())
    {
        throw emptyStringException();
        
    }
    
    this->inputString = inputString;
}