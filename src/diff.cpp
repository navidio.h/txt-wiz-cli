#include "diff.hpp"
#include <string>
#include <fstream> // To read from file
#include "CustomExceptions.hpp"
#include <iostream> // For std::cout

///////////////////////////////////////////////////////////

void diff::execute() const
{
    // Opens file one
    std::ifstream inputOne(fileNameOne, std::ios::in);
    // Opens file two
    std::ifstream inputTwo(fileNameTwo, std::ios::in);
    // Stores lines from file one
    std::string readFromFileOne;
    // Stores lines from file two
    std::string readFromFileTwo;
    while (true)
    {
        // Reads one line from file one
        std::getline(inputOne, readFromFileOne);
        // Checks if getline was successful
        if (inputOne.fail())
        {
            // EOF of file one reached
            inputOne.close();
            // Prints rest of file two
            while (true)
            {
                std::getline(inputTwo, readFromFileTwo);
                if (inputTwo.fail())
                {
                    // EOF of file two reached
                    inputTwo.close();
                    return;
                }
                
                std::cout << "2> " << readFromFileTwo << '\n';
            }
            
        }
        // Reads one line from file two
        std::getline(inputTwo, readFromFileTwo);
        // Checks if getline was successful
        if (inputTwo.fail())
        {
            // EOF of file two reached
            inputTwo.close();
            // Prints previous read line
            std::cout << "1> " << readFromFileOne << '\n';
            // Prints rest of file one
            while (true) 
            {
                std::getline(inputOne, readFromFileOne);
                if (inputOne.fail())
                {
                    // EOF of file one reached
                    inputOne.close();
                    return;
                }

                std::cout << "1> " << readFromFileOne << '\n';
            }
        }
        // Checks if the line from file one and the line from file two are identical
        if (readFromFileOne == readFromFileTwo)
        {
            // Prints identical line
            std::cout << "#> " << readFromFileOne << '\n';
        }
        // The line from file one and the line from file two are different
        else
        {
            std::cout << "1> " << readFromFileOne << '\n';
            std::cout << "2> " << readFromFileTwo << '\n';
        }
    }
    
}

///////////////////////////////////////////////////////////

void diff::setFileNames(const std::string& one, const std::string& two)
{
    // making sure none of the strings are empty
    if (one.empty() || two.empty())
    {
        throw emptyStringException();
    }
    // Checks if user's txt file of choice (file1) exists and can be opened
    std::ifstream test(one, std::ios::in);
    if (test.fail())
    {
        throw nonExistentFileException();
    }

    test.close();
    // Checks if user's txt file of choice (file2) exists and can be opened
    test.open(two, std::ios::in);
    if (test.fail())
    {
        throw nonExistentFileException();
    }

    test.close();
    fileNameOne = one;
    fileNameTwo = two;
}