#include "txtwrap.hpp"
#include <string>
#include "CustomExceptions.hpp"
#include <fstream> // To read from file
#include <sys/ioctl.h> // ioctl() and TIOCGWINSZ
#include <unistd.h> // For STDOUT_FILENO
#include <iostream> // For std::cout

///////////////////////////////////////////////////////////

void txtwrap::execute() const
{
    // Stores a string from user's text file of choice
    std::string readFromFile;
    // Opens user's text file of choice
    std::ifstream input(fileName, std::ios::in);
    // Output line position
    unsigned int position = 0;
    // -f
    if (flag)
    {
        // readFromFile index
        int rFFpos;
        while (true)
        {
            // Reads from file
            input >> readFromFile;
            // Checks if >> was successful
            if (input.fail())
            {
                // EOF reached
                break;
            }
            
            // End of the current output line reached
            if (position >= width)
            {
                // New line
                std::cout << "\n";
                // Adjustin line position
                position = 0;
            }
            // Current string fits on the same line considering input width
            if (position + readFromFile.length() <= width)
            {
                // Adjusting position
                position += readFromFile.length() + 1;
                // Printing string
                std::cout << readFromFile << ' ';
                continue;
            }
            
            // Printing a new string so readFromFile position should go back to 0
            rFFpos = 0;
            // Checks if the entire string has been printed
            while (rFFpos != readFromFile.length())
            {   
                // There's enough room for atleast one char and a '-'
                if (position < width - 1)
                {
                    // Prints current char
                    std::cout << readFromFile[rFFpos];
                    // Adjusting line and readFromFile positions
                    position++;
                    rFFpos++;
                }
                // End of the current output line reached ('-' needed)
                else if (rFFpos > 0)
                {
                    // Prints '-' and goes to new line
                    std::cout << "-\n";
                    // Adjusting line position
                    position = 0;
                }
                // End of the current output line reached ('-' not needed)
                else
                {
                    // New line
                    std::cout << '\n';
                    // Adjusting line position
                    position = 0;
                }
            }
            // Prints the space between each word
            std::cout << ' ';
            // Space needs space too
            position++;
        }

    }
    // no -f
    else
    {
        // Making sure all words have a valid length
        validateWordLengths();
        while (true)
        {
            // Reads from file
            input >> readFromFile;
            // Checks if >> was successfull
            if (input.fail())
            {
                // EOF reached
                break;
            }
            // Checks if current string fits on the current output line
            if (position + readFromFile.length() <= width)
            {
                // Adjusting line position (+1 for space between words)
                position += readFromFile.length() + 1;
            }
            // Current string does not fit on the current output line
            else
            {
                // New line
                std::cout << '\n';
                // Adjusting line position (+1 for space between words)
                position = readFromFile.length() + 1;
            }
            // Printing current string
            std::cout << readFromFile << ' ';
        }
        
    }
    
    input.close();
    std::cout << '\n';
}

///////////////////////////////////////////////////////////

void txtwrap::setFileNameAndWidth(const std::string& fileName, const std::string& width)
{
    // making sure none of the strings are empty
    if (fileName.empty() || width.empty())
    {
        throw emptyStringException();
    }
    // Checks if user's txt file of choice exists and can be opened
    std::ifstream test(fileName, std::ios::in);
    if (test.fail())
    {
        throw nonExistentFileException();
    }

    test.close();
    // Checks if input width string stores a positive integer value
    for (size_t i = 0; i < width.length(); i++)
    {
        if (!isdigit(width[i]))
        {
            throw nonExistentCommandException();
        }
        
    }
    //width must be greater than or equal to 1
    if (stoi(width) < 1)
    {
        throw nonExistentCommandException();
    }
    // Checks if input line width fits in user's terminal
    (stoi(width) <= getTerminalWidth()) ? (this->width = stoi(width)) : (throw terminalSizeException());
    this->fileName = fileName;
}

///////////////////////////////////////////////////////////

void txtwrap::setFlag()
{
    // -f
    flag = true;
}

///////////////////////////////////////////////////////////

unsigned txtwrap::getTerminalWidth() const
{
    winsize size;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
    return size.ws_col;
}

///////////////////////////////////////////////////////////

void txtwrap::validateWordLengths() const
{
    // Opens user's text file of choice
    std::ifstream input(fileName, std::ios::in);
    // Stores a string from the file
    std::string test;
    while (true)
    {
        // Reads from file
        input >> test;
        // Checks if >> was successful
        if (input.fail())
        {
            // EOF reached
            break;
        }
        // Checks if current string has a valid length
        if (test.length() > width)
        {
            throw outOfWidthException();
        }
    }

    input.close();
}