#include "evaluate.hpp"
#include <string>
#include "CustomExceptions.hpp"
#include <iostream> // For std::cout

///////////////////////////////////////////////////////////

void evaluate::execute() const
{
    // Prints result
    std::cout << stoi(number, nullptr, base) << '\n';
}

///////////////////////////////////////////////////////////

void evaluate::setNumberAndBase(const std::string& numberString, const std::string& base)
{
    // making sure none of the strings are empty
    if (numberString.empty() || base.empty())
    {
        throw emptyStringException();
    }
    // Checking if base is an integer number
    for (size_t i = 0; i < base.length(); i++)
    {
        if (!std::isdigit(base[i]))
        {
            throw nonExistentCommandException();
        }
    }
    // Checking if base is in the (2 - 36) range
    if (std::stoi(base) < 2 || std::stoi(base) > 36)
    {
        throw nonExistentCommandException();
    }

    // Checks if number string does not contain invalid chars (e.g. symbols)
    for (size_t i = 0; i < numberString.length(); i++)
    {
        // Number may be negative (e.g. "-A2")
        if (i == 0 && numberString[i] == '-')
        {
            continue;
        }
        // Checks if numberString[i] is an integer (0 - 9)
        if (isdigit(numberString[i]))
        {
            // Conversion from char to integer (- '0')
            // numberString[i] cannot be greater than or equal to base
            if (numberString[i] - '0' >= std::stoi(base))
            {
                throw nonmatchingBaseAndNumberException();
            }
            
        }
        // Checks if numberString[i] a letter from the alphabet (A-z)
        else if (std::isalpha(numberString[i]))
        {
            // Conversion from ASCII code to integer value (e.g. 'A': ASCII == 65 --> 65 - 55 = 10)
            // numberString[i] cannot be greater than or equal to base
            if ((static_cast<int>(toupper(numberString[i])) - 55) >= std::stoi(base))
            {
                throw nonmatchingBaseAndNumberException();
            }
            
        }
        
        else
        {
            throw nonExistentCommandException();
        }
    }
    
    this->number = numberString;
    this->base = stoi(base); 
}