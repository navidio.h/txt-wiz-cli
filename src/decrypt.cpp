#include "decrypt.hpp"
#include "iostream" // For std::cout
#include "CustomExceptions.hpp"
#include <fstream> // To read from file
#include <string>

///////////////////////////////////////////////////////////

void decrypt::execute() const
{
    // True if a char has already been printed (decryption was needed)
    bool printed;
    // Opens user's txt file of choice
    std::ifstream input(fileName, std::ios::in);
    // Stores chars from the txt file
    char readFromFile;
    while (true)
    {
        // Reads one char from the txt file
        input.get(readFromFile);
        // Making sure get was successful
        if (input.fail())
        {
            // EOF reached
            break;
        }
        
        for (size_t i = 0; i < publicString.length(); i++)
        {
            // Read char has not been printed yet
            printed = false;
            // Checks if read char matches any char from the publicString
            if (publicString[i] == readFromFile)
            {
                // Prints the equivalent char from the secretString
                std::cout << secretString[i];
                // Read char has been printed and should not be printed again
                printed = true;
                break;
            }

        }
        
        if (!printed)
        {
            // No decryption was necessary so the original read char is printed
            std::cout << readFromFile;
        }
        
    }
    // Closes user's txt file of choice
    input.close();
}

///////////////////////////////////////////////////////////

void decrypt::setAllStrings(const std::string& name, const std::string& pub, const std::string& sec)
{
    // making sure none of the strings are empty
    if (name.empty() || pub.empty() || sec.empty()) 
    {
        throw emptyStringException();
    }
    
    // Checks if user's txt file of choice exists and can be opened
    std::ifstream test(name, std::ios::in);
    if (test.fail())
    {
        throw nonExistentFileException();
    }

    test.close();
    // Checks if the public string and the secret string have the same length
    if (pub.length() != sec.length())
    {
        // Input command is invalid
        throw nonExistentCommandException();
    }
    
    // Checks if there are no duplicate chars in the public string
    for (size_t i = 0; i < pub.length(); i++)
    {
        for (size_t j = i + 1; j < pub.length(); j++)
        {
            if (pub[i] == pub[j])
            {
                throw nonExistentCommandException();
            }
            
        }
        
    }
    
    fileName = name;
    publicString = pub;
    secretString = sec;
}
