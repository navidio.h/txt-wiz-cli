#if !defined(__SPLIT_HPP)
#define __SPLIT_HPP

#include "command.hpp" // For inheritance
#include <string>

///////////////////////////////////////////////////////////

class split final : public command
{
private:
    // The string that should be split
    std::string input;
    std::string delimiter;
public:
    void setInputAndDelimiter(const std::string&, const std::string&); // Sets input and delimiter
    virtual void execute() const override; // Executes command
};

#endif // __SPLIT_HPP
