#if !defined(__CUSTOM_HANDLERS_HPP)
#define __CUSTOM_HANDLERS_HPP

#include <iostream>

///////////////////////////////////////////////////////////

void customNewHandler() // When new fails
{
    std::cerr << "Program faced memory deficiency or was not allowed to allocate enough memory.\n";
    std::abort();
}

#endif // __CUSTOM_HANDLERS_HPP




