#if !defined(__DECRYPT_HPP)
#define __DECRYPT_HPP

#include <string>
#include "command.hpp" // For inheritance

///////////////////////////////////////////////////////////

class decrypt final : public command
{
private:
    std::string fileName; // The file that should be read from
    std::string publicString;
    std::string secretString;
public:
    virtual void execute() const override; // Executes command
    // Sets filename, publicString, and secretString
    void setAllStrings(const std::string&, const std::string&, const std::string&); 
};


#endif // __DECRYPT_HPP
