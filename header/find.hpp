#if !defined(__FIND_HPP)
#define __FIND_HPP

#include "command.hpp" // For inheritance
#include <string>

///////////////////////////////////////////////////////////

class find final : public command
{
private:
    std::string keyString; // The string we're looking for
    std::string mainString; // The string to look for keyString in
public:
    virtual void execute() const override; // Executes command
    void setKeyAndMainStrings(const std::string&, const std::string&); // Sets keyString and mainString
};

#endif // __FIND_HPP
