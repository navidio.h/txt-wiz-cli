#if !defined(__EVALUATE_HPP)
#define __EVALUATE_HPP

#include "command.hpp" // For inheritance
#include <string>

///////////////////////////////////////////////////////////

class evaluate final : public command
{
private:
    std::string number;
    int base;
public:
    virtual void execute() const override; // Executes command
    // Sets number and base
    void setNumberAndBase(const std::string&, const std::string&);
};

#endif // __EVALUATE_HPP
