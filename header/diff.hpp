#if !defined(__DIFF_HPP)
#define __DIFF_HPP

#include <string>
#include "command.hpp" // For inheritance

///////////////////////////////////////////////////////////

class diff final : public command
{
private:
    std::string fileNameOne; // First file to read from
    std::string fileNameTwo; // Second file to read from
public:
    virtual void execute() const override; // Executes command
    void setFileNames(const std::string&, const std::string&); // Sets both fileNames
};

#endif // __DIFF_HPP
