#if !defined(__REVERSE_HPP)
#define __REVERSE_HPP

#include "command.hpp" // For inheritance
#include <string>

///////////////////////////////////////////////////////////

class reverse final : public command
{
private:
    std::string inputString; // String that should be reversed
public:
    virtual void execute() const override; // Executes command
    void setInputString(const std::string&);
};

#endif // __REVERSE_HPP
