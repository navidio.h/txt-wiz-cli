#if !defined(__CAPITALIZE_HPP)
#define __CAPITALIZE_HPP

#include "command.hpp" // For inheritance
#include <string>

///////////////////////////////////////////////////////////

class capitalize final : public command
{
private:
    std::string inputString; // The string that should be capitalized
public:
    virtual void execute() const override; // Executes command
    void setInputString(const std::string&);
};

#endif // __CAPITALIZE_HPP
