#if !defined(__INDENT_HPP)
#define __INDENT_HPP

#include "command.hpp" // For inheritance
#include <string>

///////////////////////////////////////////////////////////

class indent final : public command
{
private:
    std::string fileName; // File to read from
    std::string indentation;
    bool flag = false; // -n or no -n
    int lineNumber; // the number to start from (if -n)

public:
    virtual void execute() const override; // Executes command
    void setFileNameAndIndentation(const std::string&, const std::string&);
    void setFlagAndNumber(const std::string&); // Sets flag to true and sets lineNumber
};

#endif // __INDENT_HPP
