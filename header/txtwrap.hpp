	#if !defined(__TXTWRAP_HPP)
#define __TXTWRAP_HPP

#include <string>
#include "command.hpp" // For inheritance

///////////////////////////////////////////////////////////

class txtwrap final : public command
{
private:
    std::string fileName; // File to read from
    int width;
    bool flag = false; // -f or no -f
public:
    virtual void execute() const override; // Executes command
    void setFileNameAndWidth(const std::string&, const std::string&);
    void setFlag(); // Sets flag to true
    // Returns the number of columns of the terminal
    unsigned int getTerminalWidth() const;
    // Checks if all words from the file are shorter or equal to width in length (when there is no -f)
    void validateWordLengths() const;
};

#endif // __TXTWRAP_HPP
