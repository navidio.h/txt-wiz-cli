#if !defined(__REPLACE_HPP)
#define __REPLACE_HPP

#include "command.hpp" // For inheritance
#include <string>

///////////////////////////////////////////////////////////

class replace final : public command
{
private:
    std::string inputString; // The string that should be manipulated
    std::string oldString; // The string that should be replaced
    std::string newString; // The string that should replace oldString
public:
    virtual void execute() const override; // Executes command
    // Sets inputString, oldString, and newString
    void setAllStrings(const std::string&, const std::string&, const std::string&); 
};

#endif // __REPLACE_HPP


