#if !defined(__PANDASAYS_HPP)
#define __PANDASAYS_HPP

#include "command.hpp" // For inheritance
#include <string>

///////////////////////////////////////////////////////////

class pandasays final : public command
{
private:
    std::string sayThis;
public:
    virtual void execute() const override;
    void setSayThis(const std::string&);
};

#endif // __PANDASAYS_HPP
