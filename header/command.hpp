#if !defined(__COMMAND_HPP)
#define __COMMAND_HPP

///////////////////////////////////////////////////////////

class command
{
public:
    virtual void execute() const = 0; // Executes command
};

#endif // __COMMAND_HPP
