#if !defined(__CUSTOM_EXCEPTIONS_HPP)
#define __CUSTOM_EXCEPTIONS_HPP

#include <stdexcept>

///////////////////////////////////////////////////////////

// When user's input does not match any of the commands (capitalize, reverse, etc)
class nonExistentCommandException final : public std::runtime_error
{
public:
    nonExistentCommandException():std::runtime_error("The command you have input is invalid or does not exist."){};
};

///////////////////////////////////////////////////////////

// When an inputted string is empty ("")
class emptyStringException final : public std::runtime_error
{
public:
    emptyStringException():std::runtime_error("One or more of your input strings are empty."){};
};

///////////////////////////////////////////////////////////

// When a number doesn't match its base (command evaluate)
class nonmatchingBaseAndNumberException final : public std::runtime_error
{
public:
    nonmatchingBaseAndNumberException():std::runtime_error("The number you have input does not match input base."){};
};

///////////////////////////////////////////////////////////

// When a file that the program is supposed to read from does not exist
class nonExistentFileException final : public std::runtime_error
{
public:
    nonExistentFileException():std::runtime_error("The file you have chosen does not exist or cannot be opened."){};
};

///////////////////////////////////////////////////////////

// When user's terminal is too small for the program's output
class terminalSizeException final : public std::runtime_error
{
public:
    terminalSizeException():std::runtime_error("Your terminal does not have enough space. Try zooming out."){};
};

// When a word read from a txt file is longer than the width input by user
class outOfWidthException final : public std::runtime_error
{
public:
    outOfWidthException():std::runtime_error("Your file contains one or more words that are lengthier than your width of choice."){};
};

#endif // __CUSTOM_EXCEPTIONS_HPP
